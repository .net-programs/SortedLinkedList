﻿using System;
using System.Collections;
using System.Diagnostics;

namespace SortedLinkedList
{
    /// <summary>
    /// Прокси-тип коллекции.
    /// </summary>
    internal class CollectionDebugView
    {
        private ICollection collection;

        /// <summary>
        /// Создаёт прокси-тип для указанной коллекции.
        /// </summary>
        /// <param name="collection">Коллекция.</param>
        public CollectionDebugView(ICollection collection)
        {
            this.collection = collection ?? throw new ArgumentNullException(nameof(collection));
        }

        /// <summary>
        /// Элементы коллекции.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public object[] Items
        {
            get
            {
                object[] items = new object[collection.Count];
                collection.CopyTo(items, 0);
                return items;
            }
        }
    }
}
