﻿using System;
using System.Collections.Generic;

namespace SortedLinkedList
{

    /// <summary>
    /// Узел.
    /// </summary>
    /// <typeparam name="TKey">Тип ключей.</typeparam>
    /// <typeparam name="TValue">Тип значений.</typeparam>
    [Serializable]
    public class Node<TKey, TValue>
    {
        /// <summary>
        /// Пара-значение.
        /// </summary>
        public KeyValuePair<TKey, TValue> Pair { get; set; }

        /// <summary>
        /// Следующий узел.
        /// </summary>
        public Node<TKey, TValue> Next { get; set; }

        /// <summary>
        /// Конструирует узел на основе пары и ссылки на следующий узел.
        /// </summary>
        /// <param name="pair">Пара.</param>
        /// <param name="next">Следующий узел.</param>
        public Node(KeyValuePair<TKey, TValue> pair, Node<TKey, TValue> next = null)
        {
            Pair = pair;
            Next = next;
        }
    }
}