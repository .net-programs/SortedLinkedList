﻿using System;

namespace SortedLinkedList
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            SortedLinkedList<int, int> sortedLinkedList = new SortedLinkedList<int, int>()
            {
                { 3, 1 },
                { 4, 2 },
            };
            SortedLinkedList<int, int>.KeyCollection keys = sortedLinkedList.Keys;
            foreach (var item in keys)
                Console.Write($"{item} ");
            int[] array = new int[keys.Count];
            keys.CopyTo(array, 0);
            Console.WriteLine();
            foreach (var item in array)
                Console.Write($"{item} ");
            Console.WriteLine();
        }
    }
}
